import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line


def mapper(record):
    # input is supposed to be a list of strings that represent a tuple


def reducer(key, list_of_values):
    # supposed to make this look like a join between two tables


# Do not modify below this line
# =============================
if __name__ == '__main__':
    inputdata = open(sys.argv[1], 'r')
    mr.execute(inputdata, mapper, reducer)
